const { APNS, Notification } = require("apns2");
const fs = require("fs");

let client = new APNS({
    team: "DD4WC9JHR6",
    keyId: "4TRN95263B",
    signingKey: fs.readFileSync("/Users/hanshamm/Documents/Projects/amazon-chime-sdk-ios-master 2/voip-gateway/AuthKey_4TRN95263B.p8"),
    defaultTopic: "de.openreply.chime-sdk.demo.voip",
    host: "api.sandbox.push.apple.com"
});

class VoipNotification extends Notification {

    constructor(deviceToken) {
        super(deviceToken, {
            pushType: "voip",
            priority: 10,
            alert: "Alert Message",
            data: {
                handle: "de.openreply.chime-sdk.demo.voip.12345",
                server: "http://0afe8e6a.ngrok.io",
                meeting: "12345",
                name: "OBI Berater"
            }
        });
    }
}

client
    .send(new VoipNotification("1dc9ed8a50fb90b350b677b620d191b372ad3ae100f228843d17e5364bd4d577"))
    .then((result1, result2) => {
        console.log("hooraay");
    })
    .catch(error => {
        console.log(error)
    });


