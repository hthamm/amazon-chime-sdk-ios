//
//  AppDelegate.swift
//  AmazonChimeSDKDemo
//
//  Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
//  SPDX-License-Identifier: Apache-2.0
//

import UIKit
import PushKit
import CallKit

extension Data {
    func toTokenString() -> String {
        return self.map { String(format: "%02.2hhx", $0) }.joined()
    }
}

struct ObiCaller {
    let name: String
    let server: String
    let meetingID: String
    
    func callID() -> String {
        return "\(name) \(meetingID)"
    }
    
    func toString() -> String {
        return "\(server)/\(meetingID)/\(name)"
    }
}

protocol ObiCallDelegate {
    func receiveCall(caller: ObiCaller)
}

extension AppDelegate: CXProviderDelegate {
 
    func providerDidReset(_ provider: CXProvider) {
        print("provider did reset")
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("provider perform CXAnswerCallAction")
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("provider perform CXEndCallAction")
        action.fulfill()
    }
  
}
 
extension AppDelegate: PKPushRegistryDelegate {
    func registerForVoIPPushes() {
        self.voipRegistry = PKPushRegistry(queue: nil)
        self.voipRegistry?.delegate = self
        self.voipRegistry?.desiredPushTypes = [.voIP]
    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didUpdate pushCredentials: PKPushCredentials,
                      for type: PKPushType) {
        print("VoIP Token: \(pushCredentials.token.toTokenString())")
    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didReceiveIncomingPushWith payload: PKPushPayload,
                      for type: PKPushType) {
        
        guard type == .voIP else { return }
            
        let payloadDict = payload.dictionaryPayload
        
        print("incomming voIP notification: \(payloadDict)")
        if let server = payloadDict["server"] as? String,
            let meetingID = payloadDict["meeting"] as? String {
            let name: String = payloadDict["name"] as? String ?? "Unbekannt"
            print("connection: \(server) id: \(meetingID)")
            let obiCaller = ObiCaller(name: name, server: server, meetingID: meetingID)
            createIncommingCall(who: obiCaller)
        }   
        
    }
    
    func endCall(provider: CXProvider? = nil, who: ObiCaller) {
        guard let provider = provider ?? self.voipProvider else { return }
        let uuidString = who.toString()
        let uuid = UUID(uuidString: uuidString) ?? UUID()
        provider.reportCall(with: uuid, endedAt: Date(), reason: .remoteEnded)
    }
    
    func createIncommingCall(provider: CXProvider? = nil, who: ObiCaller) {
        guard let provider = provider ?? self.voipProvider else { return }
        let update = CXCallUpdate()
        
        let uuid = UUID(uuidString: who.toString()) ?? UUID()
            
        update.remoteHandle = CXHandle(type: .generic, value: who.callID())
        caller[uuid] = who
        
        provider.reportNewIncomingCall(with: uuid, update: update, completion: { error in
            if let error = error {
                print("Error: \(error)")
            } else {
                if let obiCaller = self.caller[uuid] {
                    self.obiCallDelegate?.receiveCall(caller: obiCaller)
                }
                
            }
        })
    }
    
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var voipRegistry: PKPushRegistry?
    var voipProvider: CXProvider?
    var obiCallDelegate: ObiCallDelegate?
    
    var caller: [UUID: ObiCaller] = [:]
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenString = deviceToken.toTokenString()
        print("Remote notification service token: \(tokenString)")
        //self.sendDeviceTokenToServer(data: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to regsiter for remote notification service: \(error)")
    }
    
    func registerObiCallerDelegate(delegate: ObiCallDelegate) {
        self.obiCallDelegate = delegate
        
        let provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "My App"))
        provider.setDelegate(self, queue: nil)
        self.voipProvider = provider
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.registerForRemoteNotifications()
        registerForVoIPPushes()
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
