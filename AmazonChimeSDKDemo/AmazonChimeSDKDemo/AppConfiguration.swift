//
//  AppConfiguration.swift
//  AmazonChimeSDKDemo
//
//  Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
//  SPDX-License-Identifier: Apache-2.0
//

import Foundation

struct AppConfiguration {
    static let url = "http://127.0.0.1:8080/"
    static let region = "eu-central-1"
}
