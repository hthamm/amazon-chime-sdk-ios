//
//  ViewController.swift
//  AmazonChimeSDKDemo
//
//  Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
//  SPDX-License-Identifier: Apache-2.0
//

import AmazonChimeSDK
import AVFoundation
import UIKit
import Toast
import CallKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


class ViewController: UIViewController {
    var currentCaller: ObiCaller? {
        guard let name = nameText?.text,
            let server = serverText?.text,
            let meetingID = meetingIDText?.text else {
                return nil
        }
        let caller = ObiCaller(name: name, server: server, meetingID: meetingID)
        return caller
    }
    
    let logger = ConsoleLogger(name: "ViewController")

    @IBOutlet weak var serverText: UITextField!
    @IBOutlet var meetingIDText: UITextField!
    @IBOutlet var nameText: UITextField!
    @IBOutlet var joinButton: UIButton!
    
    @IBAction func doneWithAllInput(_ sender: Any) {
        guard let textField = sender as? UITextField else {
            return
        }
        textField.resignFirstResponder()
    }
    
    @IBAction func didEndEditing(_ sender: Any) {
        
        guard let textField = sender as? UITextField else {
            return
        }
        
        if textField == serverText {
            meetingIDText.becomeFirstResponder()
        } else if textField == meetingIDText {
            nameText.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        nameText.text = UIDevice.current.name
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.registerObiCallerDelegate(delegate: self)
        }
    }
    
    private func joinMeeting() {
        if serverText.text?.isEmpty == true {
            serverText.text = AppConfiguration.url
        }
        
        guard let caller = self.currentCaller else {
            return
        }
     
        postRequest(completion: { data, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.view.makeToast("\(error.localizedDescription)", duration: 2.0)
                }
                return
            }
            
            if let data = data {
                let (meetingResp, attendeeResp) = self.processJson(data: data)
                
                guard
                    let currentMeetingResponse = meetingResp,
                    let currentAttendeeResponse = attendeeResp
                else {
                    return
                }
                
                let meetingSessionConfig = MeetingSessionConfiguration(
                    createMeetingResponse: currentMeetingResponse,
                    createAttendeeResponse: currentAttendeeResponse)
                
                DispatchQueue.main.async {
                    let meetingView = UIStoryboard(name: "Main", bundle: nil)
                    guard let meetingViewController = meetingView.instantiateViewController(withIdentifier: "meeting")
                        as? MeetingViewController else {
                            self.logger.error(msg: "Unable to instantitateViewController")
                            return
                    }
                    meetingViewController.modalPresentationStyle = .fullScreen
                    meetingViewController.meetingSessionConfig = meetingSessionConfig
                    meetingViewController.caller = caller
                    self.present(meetingViewController, animated: true, completion: nil)
                }
            }
        })
    }
    
    @IBAction func joinMeeting(sender: UIButton) {
        joinMeeting()
    }

    private func postRequest(completion: @escaping CompletionFunc) {
        guard let caller = self.currentCaller else {
            DispatchQueue.main.async {
                self.view.makeToast("Given empty meetingID or name please provide those values", duration: 2.0)
            }
            return
        }
        
        var serverUrl: String = ""
        if caller.server.count > 0 {
            serverUrl = caller.server
        } else {
            serverUrl = AppConfiguration.url
        }
        if !serverUrl.hasSuffix("/") {
            serverUrl.append("/")
        }
        let name = self.nameText.text ?? ""
        
        let encodedURL = HttpUtils.encodeStrForURL(
            str: "\(serverUrl)join?title=\(caller.meetingID)&name=\(name)&region=\(AppConfiguration.region)"
        )
        HttpUtils.postRequest(
            url: encodedURL,
            completion: completion,
            jsonData: nil, logger: logger)
    }

    private func processJson(data: Data) -> (CreateMeetingResponse?, CreateAttendeeResponse?) {
        let jsonDecoder = JSONDecoder()
        do {
            let meetingResponse = try jsonDecoder.decode(MeetingResponse.self, from: data)
            let meetingResp = CreateMeetingResponse(meeting:
                Meeting(
                    meetingId: meetingResponse.joinInfo.meeting.meetingId,
                    mediaPlacement: MediaPlacement(
                        audioFallbackUrl: meetingResponse.joinInfo.meeting.mediaPlacement.audioFallbackUrl,
                        audioHostUrl: meetingResponse.joinInfo.meeting.mediaPlacement.audioHostUrl,
                        turnControlUrl: meetingResponse.joinInfo.meeting.mediaPlacement.turnControlUrl,
                        signalingUrl: meetingResponse.joinInfo.meeting.mediaPlacement.signalingUrl
                    )
                )
            )
            let attendeeResp = CreateAttendeeResponse(attendee:
                Attendee(attendeeId: meetingResponse.joinInfo.attendee.attendeeId,
                         joinToken: meetingResponse.joinInfo.attendee.joinToken))

            return (meetingResp, attendeeResp)
        } catch {
            logger.error(msg: error.localizedDescription)
            return (nil, nil)
        }
    }
}


extension ViewController: ObiCallDelegate {
    func endCall(caller: ObiCaller) {
        
    }
    func receiveCall(caller: ObiCaller) {
        self.meetingIDText?.text = caller.meetingID
        self.serverText?.text = caller.server
        self.joinMeeting()
    }
}
